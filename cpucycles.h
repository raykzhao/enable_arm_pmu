/*
cpucycles cortex.h version 20100912
D. J. Bernstein
Public domain.
*/

#ifndef CPUCYCLES_cortex_h
#define CPUCYCLES_cortex_h

long long cpucycles(void);

#endif
